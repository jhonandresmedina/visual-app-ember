import Ember from 'ember';

export default Ember.Component.extend({
  //tagName: 'svg',
  margin: {top: 60, right: 30, bottom: 50, left: 150},
  draw: function(){
    var formatPercent = d3.format("");
    var width = this.get('width');
    var height = this.get('height');
    var marginLeft = this.get('margin.left');
    var marginRight = this.get('margin.right');
    var marginTop = this.get('margin.top');
    var marginBottom = this.get('margin.bottom');
    var data = this.get('data');

    var svg = d3.select('#'+this.get('elementId'));

    //rangeBands([0, height], 0.15) último número representa la separación entre las barras
    var x = d3.scale.linear().range([0, width]);
    var y = d3.scale.ordinal().rangeBands([0, height], 0.15);

    var xAxis = d3.svg.axis().scale(x).orient("top").tickFormat(formatPercent);
    var yAxis = d3.svg.axis().scale(y).orient("left");

    var svg = d3.select("body").append("svg")
      .attr("width", width + marginLeft + marginRight)
      .attr("height", height + marginTop + marginBottom)
      .append("g")
        .attr("transform", "translate(" + marginLeft + "," + marginTop + ")");

    data.forEach(function(d) {
        d.frequency = +d.frequency;
    });

    x.domain([0, d3.max(data, function(d) { return d.frequency; })]);
    y.domain(data.map(function(d) { return d.nombre_autor; }));

    //adding x axis
    svg.append("g")
      .attr("class", "x axis")
      .call(xAxis)
      .append("text")
        .attr("x", (width/2))
        .attr("y", -marginTop)
        .attr("dy", "1em")
        .style("text-anchor", "middle")
        .style("font-size", "18px")
        .text("Publicaciones por autor");

    //adding y axis
    svg.append("g")
      .attr("class", "y axis")
      .call(yAxis);

    svg.selectAll('.bar')
      .data(data)
      .enter()
      .append('rect')
      .attr("class", "bar")
      .attr('width', 0)
      .attr("y", function(d) { return y(d.nombre_autor); })
      .attr("height", y.rangeBand())
        //.on('mouseover', tip.show)
        //.on('mouseout', tip.hide)
      .transition()
        .attr("width", function(d) {
          return x(d.frequency);
        })
        .delay( function(d, i) {
          return i * 40;
        })
        .duration(800);
        //.ease('elastic');
  }, //draw function
  didInsertElement: function(){
    this.draw();
  }
});
